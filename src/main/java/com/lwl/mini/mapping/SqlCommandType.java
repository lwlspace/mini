package com.lwl.mini.mapping;

/**
 * @author lwl
 * @create 2022/4/13 13:54
 */
public enum SqlCommandType {
    INSERT,UPDATE,SELECT,DELETE;
}
