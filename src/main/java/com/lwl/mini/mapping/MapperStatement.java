package com.lwl.mini.mapping;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.lang.reflect.Method;
import java.util.Map;

/**
 * @author lwl
 * @create 2022/4/18 11:01
 */
@Data
@AllArgsConstructor
public class MapperStatement {

    /**
     * SQL语句
     */
    private String statement;

    /**
     * key:@param配置的值，value为具体传参值
     */
    private Map<Object, Object> paramMap;

    /**
     * 返回值类型
     */
    private Class<? extends Object> returnType;

    /**
     * 执行方法
     */
    private Method method;

    /**
     * @return 生成缓存中的key
     * 即 sql+参数+返回值类信息
     */
    public String  generateKey() {
        String key = statement;
        String part = " ";
        if(paramMap != null && !paramMap.isEmpty()){
            for (Object param:paramMap.values()){
                part = part + param;
            }
        }
        key = key+part+returnType.toString();

        return key.replaceAll(" ","");
    }
}
