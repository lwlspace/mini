package com.lwl.mini.builder;

import com.lwl.mini.exceptions.PersistenceException;

/**
 * @author lwl
 * @create 2022/3/15 18:43
 */
public class BuilderException extends PersistenceException {

    public BuilderException(String message) {
        super(message);
    }

    public BuilderException(String message, Throwable cause){
        super(message,cause);
    }
}
