package com.lwl.mini.builder.annotation;

import com.lwl.mini.binding.SQLInfo;
import com.lwl.mini.binding.MapperProxyFactory;
import com.lwl.mini.binding.MethodInfo;
import com.lwl.mini.util.ObjectUtils;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * @author lwl
 * @create 2022/4/13 10:34
 */
public class MapperAnnotationBuilder {

    private final MapperProxyFactory proxyFactory;
    private final Class<?> type;
    private final Set<Class<? extends Annotation>> sqlAnnotationTypes = new HashSet<>();

    public <T> MapperAnnotationBuilder(MapperProxyFactory proxyFactory, Class<T> type) {
        this.proxyFactory = proxyFactory;
        this.type = type;
        sqlAnnotationTypes.add(Select.class);
        sqlAnnotationTypes.add(Update.class);
        sqlAnnotationTypes.add(Delete.class);
        sqlAnnotationTypes.add(Insert.class);

    }


    /**
     * 通过法类信息解析出方法返回值，sql语句，参数列表信息。
     */
    public void parse(){
        Map<Method, MethodInfo> methodInfoMap = proxyFactory.getMethodInfoMap();

        Method[] methods = type.getMethods();
        for(Method method:methods){
            //获取方法sql信息
            SQLInfo sql = new SQLInfo().evalMethod(method);
            //获取返回值类型
            Class<?> type = getType(method);
            //获取方法参数列表类信息
            Class<?>[] pClass = getParameter(method);
            MethodInfo info = new MethodInfo(method,sql,type,pClass);
            methodInfoMap.put(method,info);
        }
    }

    /**
     * @return 方法返回值类信息
     */
    public Class<? extends Object> getType(Method method){
        Class<?> returnType = method.getReturnType();
        return returnType;
    }

    /**
     * @return 方法参数类信息
     */
    public Class<?>[] getParameter(Method method){
        return method.getParameterTypes();
    }

}
