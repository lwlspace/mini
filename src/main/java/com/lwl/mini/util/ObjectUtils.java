package com.lwl.mini.util;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author lwl
 * @create 2022/4/10 13:17
 */
public class ObjectUtils {
    public static boolean StringIsEmpty(String str){
        if(str == null  || str.length() < 1){
            return true;
        }
        return false;
    }

    public static boolean ArrayIsEmpty(Object[] arr){
        if(arr == null || arr.length < 1){
            return true;
        }
        return false;
    }

    public static String currentTime(){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date(System.currentTimeMillis());
        return sdf.format(date);
    }
}
