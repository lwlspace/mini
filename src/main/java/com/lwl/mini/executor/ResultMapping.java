package com.lwl.mini.executor;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

/**
 * 方便后期扩展维护
 * 实体类方法和数据库对应列名的映射
 * @author lwl
 * @create 2022/4/21 10:48
 */
@Data
@AllArgsConstructor
public class ResultMapping {

    /**
     * 数据库对应列名
     */
    private String columnLabel;
    /**
     * 列名对应实体类的字段
     */
    private Field  field;
    /**
     * 需要执行的方法
     */
    private Method method;

}
