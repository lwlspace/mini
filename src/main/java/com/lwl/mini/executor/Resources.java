package com.lwl.mini.executor;

import com.lwl.mini.builder.BuilderException;

import java.io.*;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Properties;

/**
 * @author lwl
 * @create 2022/4/9 12:10
 */
public class Resources {

    static ClassLoader classLoader = Resources.class.getClassLoader();

    public static Reader getResourceAsStream(String path) {
        try {
            URL url = classLoader.getResource(path);
            if(url != null){
                return new FileReader(new File(url.toURI()));
            }
            return null;
        }catch (Exception e){
            throw new BuilderException("The error cause:"+e.toString(),e);
        }
    }

    public static Properties getResourceAsProperties(String path){
        try {
            Properties properties = new Properties();
            properties.load(classLoader.getResourceAsStream(path));
            return properties;
        }catch (Exception e){
            throw new BuilderException("The error cause:"+e.toString(),e);
        }
    }

}
