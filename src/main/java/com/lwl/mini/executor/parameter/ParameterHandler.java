package com.lwl.mini.executor.parameter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Statement;

/**
 * @author lwl
 * @create 2022/4/18 12:49
 */
public interface ParameterHandler {

    /**
     * 预处理sql语句
     */
    PreparedStatement prepare(Connection connection);
}
