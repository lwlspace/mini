package com.lwl.mini.executor;

import com.lwl.mini.exceptions.PersistenceException;

/**
 * @author lwl
 * @create 2022/4/19 14:32
 */
public class ExecutorException extends PersistenceException {

    public ExecutorException(String message) {
        super(message);
    }

    public ExecutorException(Throwable e){
        super(e);
    }

    public ExecutorException(String message, Throwable cause) {
        super(message, cause);
    }
}
