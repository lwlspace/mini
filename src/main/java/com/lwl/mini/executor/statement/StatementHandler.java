package com.lwl.mini.executor.statement;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;


/**
 * @author lwl
 * @create 2022/4/18 12:45
 */
public interface StatementHandler {

    /**
     * 预处理sql语句
     */
    PreparedStatement prepare(Connection connection);

    ResultSet executeQuery(PreparedStatement stmt);

    Object handlerResult(ResultSet resultSet);
}
