package com.lwl.mini.executor.statement;

import com.lwl.mini.executor.ExecutorException;
import com.lwl.mini.executor.parameter.DefaultParameterHandler;
import com.lwl.mini.executor.parameter.ParameterHandler;
import com.lwl.mini.executor.resultSet.DefaultResultSetHandler;
import com.lwl.mini.executor.resultSet.ResultSetHandler;
import com.lwl.mini.mapping.MapperStatement;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 * @author lwl
 * @create 2022/4/18 12:48
 */
public class DefaultStatementHandler implements StatementHandler{

    final MapperStatement mapperStatement;

    final ParameterHandler parameterHandler;
    final ResultSetHandler resultSetHandler;

    public DefaultStatementHandler(MapperStatement mapperStatement){
        this.mapperStatement = mapperStatement;
        this.parameterHandler = new DefaultParameterHandler(mapperStatement);
        this.resultSetHandler = new DefaultResultSetHandler(mapperStatement);
    }

    @Override
    public PreparedStatement prepare(Connection connection) {
        return parameterHandler.prepare(connection);
    }

    @Override
    public ResultSet executeQuery(PreparedStatement stmt) {
        return resultSetHandler.executeQuery(stmt);
    }

    @Override
    public Object handlerResult(ResultSet resultSet) {
        try {
            return resultSetHandler.handlerResultSet(resultSet);
        } catch (Exception e) {
            throw new ExecutorException("解析结果集出现异常: "+e,e);
        }
    }
}
