package com.lwl.mini.executor;

import com.lwl.mini.mapping.MapperStatement;

import java.util.List;

/**
 * @author lwl
 * @create 2022/4/15 9:56
 */
public interface Executor {

    <E> List<E> query(MapperStatement statement);

    <E> List<E> doQuery(MapperStatement statement);

    void close();

    void commit();

    List<Integer> doUpdate(MapperStatement mapperStatement);
}
