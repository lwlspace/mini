package com.lwl.mini.executor.resultSet;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 * @author lwl
 * @create 2022/4/18 12:50
 */
public interface ResultSetHandler {
    /**
     * 执行SQL
     */
    ResultSet executeQuery(PreparedStatement statement);

    /**
     * 处理结果集
     */
    Object handlerResultSet(ResultSet result) throws Exception;
}
