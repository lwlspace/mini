package com.lwl.mini.plugin;

/**
 * @author lwl
 * @create 2022/4/15 10:08
 */
public interface Filter {

    public Object doFilter(Object target);

}
