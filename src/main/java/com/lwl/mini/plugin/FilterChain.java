package com.lwl.mini.plugin;

import java.util.ArrayList;
import java.util.List;

/**
 * @author lwl
 * @create 2022/4/15 10:08
 */
public class FilterChain {

    List<Filter> chain = new ArrayList<>();

    public Object doFilter(Object target) {
        for (Filter filter : chain) {
            target = filter.doFilter(target);
        }
        return target;
    }

    public void addFilter(Filter filter){
        chain.add(filter);
    }
}
