package com.lwl.mini.exceptions;

/**
 * @author lwl
 * @create 2022/3/15 10:50
 */
public class PersistenceException extends IbatisException{

    public PersistenceException(){
        super();
    }

    public PersistenceException(String message){
        super(message);
    }

    public PersistenceException(Throwable cause){
        super(cause);
    }

    public PersistenceException(String message,Throwable cause){
        super(message,cause);
    }

}
