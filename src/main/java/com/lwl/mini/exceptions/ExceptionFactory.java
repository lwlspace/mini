package com.lwl.mini.exceptions;

import com.lwl.mini.executor.ErrorContext;

/**
 * @author lwl
 * @create 2022/3/15 10:48
 */
public class ExceptionFactory {

    private ExceptionFactory(){
        //防止实例化
    }

    public static RuntimeException wrapException(String message,Exception e){
        return new PersistenceException(ErrorContext.instance().message(message).cause(e).toString(),e);
    }

}
