package com.lwl.mini.exceptions;

/**
 * @author lwl
 * @create 2022/3/15 10:51
 */
public class IbatisException extends RuntimeException{

    public IbatisException(String message, Throwable cause) {
        super(message,cause);
    }

    public IbatisException(String message) {
        super(message);
    }

    public IbatisException(Throwable cause){
        super(cause);
    }

    public IbatisException() {

    }
}
