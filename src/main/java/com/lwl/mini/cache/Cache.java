package com.lwl.mini.cache;

/**
 * @author lwl
 * @create 2022/4/18 10:38
 */
public interface Cache {

    /**
     * @return cache长度
     */
    Integer size();

    /**
     * 放入缓存
     */
    void put(String key,Object value);

    /**
     * 拿出缓存
     */
    Object get(String key);

}
