package com.lwl.mini.cache;

import java.util.HashMap;
import java.util.Map;

/**
 * @author lwl
 * @create 2022/4/18 10:44
 */
public class LocalCache implements Cache{

    private final static LocalCache LOCAL_CACHE = new LocalCache();

    private LocalCache(){

    }

    public static LocalCache getInstance(){
        return LOCAL_CACHE;
    }

    private Map<String,Object> cache = new HashMap<>();

    @Override
    public Integer size() {
        return cache.size();
    }

    @Override
    public void put(String key, Object value) {
        cache.put(key,value);
    }

    @Override
    public Object get(String key) {
        return cache.get(key);
    }
}
