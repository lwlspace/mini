package com.lwl.mini.binding;

import com.lwl.mini.session.SqlSession;

import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author lwl
 * @create 2022/4/12 9:57
 */
public class MapperProxyFactory<T> {

    /**
     * 当前类信息
     */
    private final Class<T> mapperInterface;
    /**
     * key为方法类信息，value为方法详情
     */
    private final Map<Method,MethodInfo> methodInfoMap = new ConcurrentHashMap<>();

    public MapperProxyFactory(Class<T> mapperInterface){
        this.mapperInterface = mapperInterface;
    }

    public Map<Method, MethodInfo> getMethodInfoMap() {
        return methodInfoMap;
    }

    public T newInstance(SqlSession sqlSession){
        final MapperProxy<T> mapperProxy = new MapperProxy<>(sqlSession,mapperInterface,methodInfoMap);
        return newInstance(mapperProxy);
    }

    public T newInstance(MapperProxy<T> mapperProxy){
        return (T)Proxy.newProxyInstance(mapperInterface.getClassLoader(),new Class[]{mapperInterface},mapperProxy);
    }
}
