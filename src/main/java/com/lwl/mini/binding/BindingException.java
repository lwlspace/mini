package com.lwl.mini.binding;

import com.lwl.mini.exceptions.PersistenceException;

/**
 * @author lwl
 * @create 2022/4/12 12:47
 */
public class BindingException extends PersistenceException {

    public BindingException() {
        super();
    }

    public BindingException(String message) {
        super(message);
    }

    public BindingException(String message, Throwable cause) {
        super(message, cause);
    }

    public BindingException(Throwable cause) {
        super(cause);
    }
}
