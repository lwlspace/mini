package com.lwl.mini.transaction;

import com.lwl.mini.mapping.Environment;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * @author lwl
 * @create 2022/4/14 13:46
 */
public interface TransactionFactory {

    Transaction newTransaction(Connection connection,boolean autoCommit,TransactionIsolationLevel level);

    Transaction newTransaction(DataSource dataSource,boolean autoCommit,TransactionIsolationLevel level) ;
}
