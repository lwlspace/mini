package com.lwl.mini.transaction;

import com.lwl.mini.exceptions.ExceptionFactory;
import com.sun.corba.se.pept.transport.ConnectionCache;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * @author lwl
 * @create 2022/4/14 14:55
 */
public class JDBCTransaction implements Transaction {

    private Connection connection;
    private final boolean autoCommit;
    private final TransactionIsolationLevel level;

    public JDBCTransaction(Connection connection, boolean autoCommit, TransactionIsolationLevel level) {
        this.connection = connection;
        this.autoCommit = autoCommit;
        this.level = level;
    }

    @Override
    public Connection getConnection() {
        try {
            //手动提交事务或自动提交事务
            connection.setAutoCommit(autoCommit);
            if (level != null) {
                //传入隔离级别
                connection.setTransactionIsolation(level.getLevel());
            }
            return connection;
        } catch (SQLException e) {
            throw ExceptionFactory.wrapException("传入connection隔离级别时出错: " + e, e);
        }
    }

    @Override
    public void close() throws SQLException {
        if (connection != null) {
            resetAutoCommit();
            connection.close();
        }
    }

    @Override
    public void commit() throws SQLException {
        if (connection != null) {
            connection.commit();
        }
    }

    public void resetAutoCommit() {
        try {
            if (!connection.getAutoCommit()) {
                connection.setAutoCommit(true);
            }
        } catch (SQLException e) {
            throw ExceptionFactory.wrapException("重置连接自动提交出错" + e, e);
        }
    }
}
