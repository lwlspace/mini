package com.lwl.mini.transaction;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * @author lwl
 * @create 2022/4/14 13:52
 */
public interface Transaction {
    Connection getConnection();

    void close() throws SQLException;

    void commit() throws SQLException;
}
