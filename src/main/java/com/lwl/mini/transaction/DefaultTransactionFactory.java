package com.lwl.mini.transaction;

import com.lwl.mini.binding.BindingException;
import com.lwl.mini.mapping.Environment;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * @author lwl
 * @create 2022/4/14 13:46
 */
public class DefaultTransactionFactory implements TransactionFactory {

    private final static TransactionFactory INSTANCE = new DefaultTransactionFactory();

    private DefaultTransactionFactory() {

    }

    public static TransactionFactory getInstance() {
        return INSTANCE;
    }

    @Override
    public Transaction newTransaction(Connection connection, boolean autoCommit, TransactionIsolationLevel level) {
        return new JDBCTransaction(connection, autoCommit, level);
    }

    @Override
    public Transaction newTransaction(DataSource dataSource, boolean autoCommit, TransactionIsolationLevel level) {
        return new DefaultTransaction(dataSource, autoCommit, level);
    }
}
