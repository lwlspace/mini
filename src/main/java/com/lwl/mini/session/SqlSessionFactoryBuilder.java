package com.lwl.mini.session;

import com.lwl.mini.builder.XMLConfigBuilder;
import com.lwl.mini.executor.Resources;

import java.io.Reader;

/**
 * @author lwl
 * @create 2022/4/9 12:02
 */
public class SqlSessionFactoryBuilder {

    public  SqlSessionFactory build(String path){
        return build(Resources.getResourceAsStream(path));
    }

    public  SqlSessionFactory build(Reader reader){
        XMLConfigBuilder xmlConfigBuilder = new XMLConfigBuilder(reader);
        return build(xmlConfigBuilder.parse());
    }

    public  SqlSessionFactory build(Configuration configuration){
        return new DefaultSqlSessionFactory(configuration);
    }
}
