package com.lwl.mini.session;

import java.io.Closeable;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Map;

/**
 * @author lwl
 * @create 2022/4/12 8:56
 */
public interface SqlSession extends Closeable {

    <T> T getMapper(Class<T> type);


    <E> List<E> selectList(String statement, Map<Object,Object> paramMap, Class<? extends Object> returnType, Method method);

     List<Integer> update(String statement, Map<Object, Object> paramMap, Class<? extends Object> returnType, Method method);

     void commit();
}
