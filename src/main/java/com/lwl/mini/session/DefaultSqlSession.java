package com.lwl.mini.session;

import com.lwl.mini.executor.Executor;
import com.lwl.mini.mapping.MapperStatement;

import java.io.IOException;
import java.lang.reflect.Member;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Map;

/**
 * @author lwl
 * @create 2022/4/15 10:38
 */
public class DefaultSqlSession implements SqlSession {

    private final Configuration configuration;
    private final Executor executor;
    private final boolean autoCommit;

    public DefaultSqlSession(Configuration configuration, Executor executor, boolean autoCommit) {
        this.configuration = configuration;
        this.executor = executor;
        this.autoCommit = autoCommit;
    }

    /**
     * @return 获取相应的类信息
     */
    @Override
    public <T> T getMapper(Class<T> type) {
        return configuration.<T>getMapper(type,this);
    }

    /**
     * 执行select查询
     */
    @Override
    public <E> List<E> selectList(String statement, Map<Object,Object> paramMap, Class<? extends Object> returnType, Method method) {
        //降低代码耦合
        MapperStatement mapperStatement = configuration.newMapperStatement(statement,paramMap,returnType,method);
        List<E> query = executor.query(mapperStatement);
        return query;
    }

    /**
     * 执行删除 更新 新增
     */
    @Override
    public List<Integer> update(String statement, Map<Object, Object> paramMap, Class<?> returnType, Method method) {
        MapperStatement mapperStatement = configuration.newMapperStatement(statement,paramMap,returnType,method);
        List<Integer> update = executor.doUpdate(mapperStatement);
        return update;
    }

    @Override
    public void close(){
        executor.close();
    }

    @Override
    public void commit() {
        executor.commit();
    }
}
