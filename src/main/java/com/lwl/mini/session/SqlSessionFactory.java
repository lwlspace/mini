package com.lwl.mini.session;

import com.lwl.mini.transaction.TransactionIsolationLevel;

/**
 * @author lwl
 * @create 2022/4/9 11:58
 */
public interface SqlSessionFactory {

    SqlSession openSession();

    SqlSession openSession(boolean autoCommit);


    /**
     * @return 基于Druid或JDBC连接池生成的SqlSession
     */
    SqlSession openSessionByDataSourceOrJDBC(boolean autoCommit, TransactionIsolationLevel level);

    /**
     * @return 基于JDBC生成的SqlSession
     */
/*    SqlSession openSessionByJDBC(boolean autoCommit,TransactionIsolationLevel level);*/
}
