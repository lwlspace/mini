package com.lwl.mini.parsing;

import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * @author lwl
 * @create 2022/4/9 12:44
 */
public class XNode {
    public final Node node;
    private final XPathParser parser;

    public XNode(Node node,XPathParser parser){
        this.node = node;
        this.parser = parser;
    }

    public XNode evalNode(String expression){
        return this.parser.evalNode(node,expression);
    }

    /**
     * 获取属性节点
     * 如:<properties resource="db.properties"/>
     * 获取name-resource和 value-db.properties
     */
    public NamedNodeMap getAttributes(){
        return node.getAttributes();
    }

    public NodeList getChildren(){
        return node.getChildNodes();
    }
}
