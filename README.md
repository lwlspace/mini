# Mini

#### 介绍
Mini<1.0>-半自动映射框架，使用方面与Mybtis大致相同，
但去除了XML的形式写sql语句，全部改为注解的形式了。
#### 软件架构
主要参考Mybatis的架构，
整体分为了四步
- 第一步： SqlSessionFactory build = new SqlSessionFactoryBuilder().build("miniConfig.xml"); // 加载配置-
- 第二步:  SqlSession sqlSession = build.openSession(true); // 获取连接-
- 第三步:  UserMapper mapper = sqlSession.getMapper(UserMapper.class); // 获取执行方法信息封装为代理对象
- 第四步:  Integer integer = mapper.deleteOne(82); //  执行方法并封装结果集

对于Mybatis框架的详解，这里放上我的文章:[Mybatis架构详解](https://blog.csdn.net/killbibi/article/details/120456393?spm=1001.2014.3001.5501)

#### 安装教程

1.  将com文件夹直接放入本地仓库的根目录下
2.  maven配置下，直接在pom.xml文件引入    
      
```
        <dependency>
            <groupId>com.lwl</groupId>
            <artifactId>Mini</artifactId>
            <version>1.0-SNAPSHOT</version>
        </dependency>
```

3. pom.xml中导入MySQL的连接jar包

#### 使用说明

 ```
        SqlSessionFactory build = new SqlSessionFactoryBuilder().build("miniConfig.xml");
        SqlSession sqlSession = build.openSession();
        UserMapper mapper = sqlSession.getMapper(UserMapper.class);
        List<User> users = mapper.selectList();
        System.out.println("users = " + users);
 ```

 **1.  SqlSessionFactory build = new SqlSessionFactoryBuilder().build("miniConfig.xml");** 
这里注意:
配置文件格式如下:

```
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE configuration[

        <!ELEMENT configuration (properties?,environment?,settings?,mappers?)>

        <!ELEMENT properties (property*)>
        <!ATTLIST properties
                resource CDATA #IMPLIED
                >

        <!ELEMENT property EMPTY>
        <!ATTLIST property
                name CDATA #REQUIRED
                value CDATA #REQUIRED
                >
        <!ELEMENT environment (dataSource)>
        <!ELEMENT dataSource (property*)>

        <!ELEMENT settings (setting*)>
        <!ELEMENT setting EMPTY>
        <!ATTLIST setting
                name CDATA #REQUIRED
                value CDATA #REQUIRED
                >
        <!ELEMENT mappers (package*)>
        <!ELEMENT package EMPTY>
        <!ATTLIST package
                name CDATA #REQUIRED
                >
        ]>
<configuration>
    <properties resource="db.properties">
    </properties>

    <settings>
        <setting name="userDruid" value="true"/>
        <setting name="cacheEnable" value="true"/>
        <setting name="loggedEnable" value="true"/>
    </settings>
    <!--<environment>
        <dataSource>
            <property name="jdbc.url" value="jdbc:mysql://localhost:3306/mybatis01"/>
            <property name="jdbc.username" value="root"/>
            <property name="jdbc.password" value="root"/>
            <property name="jdbc.driver" value="com.mysql.jdbc.Driver"/>
        </dataSource>
    </environment>-->
    <mappers>
        <package name="com.lwl.mini.mapper"/>
    </mappers>
</configuration>

```
- 配置文件开头的约束是必须加上的，这里是XML语言规范。


-  **标签说明** ：
 - properties：
    - 使用它有两种方式可以加载配置，第一种在resource属性种指定resources下的配置文件，第二种在标签内部使用property标签加载:

   -     <properties>
               <property name="jdbc.url" value="jdbc:mysql://localhost:3306/mybatis01"/>
               <property name="jdbc.username" value="root"/>
               <property name="jdbc.password" value="root"/>
               <property name="jdbc.driver" value="com.mysql.jdbc.Driver"/>
         </properties>

 - environment:
    - 该标签使用如注解所示
 - *注：这里properties和environment只能存在一个！共存时将报错。
 - settings:
   -  目前只支持三种子标签属性
         
            <setting name="userDruid" value="true"/>：框架默认使用Druid连接池管理连接，当指定为false时，则使用JDBC，不使用连接池.
            <setting name="cacheEnable" value="true"/>：是否开启缓存，这里只有一级缓存，默认开启。
            <setting name="loggedEnable" value="true"/>: 是否记录SQL语句的执行，默认开启。
- mappers：
  -   在package子标签种指定mapper包路径，会自动将包下的map进行注册并类加载获得类信息
  -   Mapper的约束 
      - 目前版本方法的参数列表，都需要使用@param指定key 用于替换sql种的#{key}
      - 暂不支持实体类的传参形式
      - 目前返回值只支持List，Integer，实体类类型等，并不支持map。
      - 使用例子如下:
```

    @Select("select * from user where id = #{testId} and username = #{name}")
    List<User> selectAll(@Param("testId") int id, @Param("name") String name);

    @Select("select * from user where id = #{testId} and username = #{name}")
    User selectOne(@Param("testId") int id, @Param("name") String name);

    @Insert("insert into user values(#{id},#{name},#{birthday},#{sex},#{address})")
    Integer insertOne(@Param("id") Integer id, @Param("name") String name, @Param("birthday") Timestamp birthday, @Param("sex") String sex, @Param("address") String address);

    @Delete("delete from user where id = #{id}")
    Integer deleteOne(@Param("id") Integer id);

    @Update("update user set username =#{name},birthday = #{birthday} where id =#{id}")
    Integer updateOne(@Param("name") String name, @Param("birthday") Timestamp birthday, @Param("id") Integer id);

```

#### 使用参考和当前版本的限制

- 1.  使用参考:[Mini](https://blog.csdn.net/killbibi/article/details/124343375)
- 2.  当前版本传参无法传入实体类进行解析，返回值只支持实体类类型和List，且select查询暂不支持一对多查询，仅支持注解的形式。
#### 后言
该项目开发测试由我完成，若存在Bug或不足，可告知我，谢谢~ 微信号:L2574308236
